# README

Workshop given for GitLab Contribute 2019: https://gitlab.com/gitlabcontribute/new-orleans/issues/52

## Development

First make sure you have Docker installed. Run `docker-compose up` in the root of the directory and visit `127.0.0.1:3000` to access the web application.

### How to run rspec

Create a test database

```
$ docker-compose exec web bundle exec rake db:create RAILS_ENV=test
```

Run all spec

```
$ docker-compose exec web bundle exec rspec spec
```

Sample output

```
shinya@shinya-MS-7A34:~/workspace/ten-things-to-know-about-gitlab-ci$ docker-compose exec web bundle exec rspec spec
..

Finished in 0.00927 seconds (files took 0.43349 seconds to load)
2 examples, 0 failures
```
